<?php
require_once __DIR__ . '/App.php';
function getApp() {
	static $app = NULL;
	
	if (empty($app)) {
		$app = new App(dirname(dirname(__FILE__)) . '/config/config.php');
	}
	
	return $app;
}

function appUrl($path = NULL) {
	if (empty($path)) {
		$path = '/';
	} elseif ($path[0] != '/') {
		$path = '/' . $path;
	}
	return getApp()->getConfig('baseurl') . $path;
}

function render($path, $data = []) {
	extract($data);
	include dirname(__DIR__) . '/views/' . $path;
}

function redirect($redirect_url) {
	render('redirect.php', [ 'redirect_url' => $redirect_url ]);
	exit();
}
