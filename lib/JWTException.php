<?php

// generic class for JWT Exceptions
class JWTException extends RuntimeException {}

// exceptions caused by undecodable garbage
class JWTGarbageException extends JWTException {}

// exception caused by unsupported encoding algorithm
class JWTUnsupportedAlgorithmException extends JWTException {}
