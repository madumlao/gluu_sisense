<?php

// generic class for OpenID Connect Exceptions
class OicException extends RuntimeException {}

// exceptions caused by connection errors
class OicConnectionException extends OicException {}

// exceptions caused by invalid client input or flow breaking
class OicClientException extends OicException {}

// exceptions caused by server errors
class OicServerException extends OicException {}

// exceptions caused by garbage server response
class OicGarbageException extends OicException {}

// exceptions caused by protocol error responses
class OicErrorException extends OicException {}
