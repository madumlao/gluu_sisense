<?php

// generic class for OpenID Connect Exceptions
class SisenseException extends RuntimeException {}

// exceptions caused by garbage parameter input response
class SisenseGarbageException extends SisenseException {}
