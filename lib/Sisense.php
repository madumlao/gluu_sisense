<?php
require_once __DIR__ . '/common.php';

class Sisense {
	// class members
	public $sub; # email of user
	public $iat; # issued at time
	public $exp; # expiry
	public $jti; # random nonce

	/**
	 * Creates a new sisense connector
	 *
	 * @param array $opts
	 *   @option string 'sub'  user email to login as
	 *   @option int 'iat'     Issued at time, Unix seconds, defaults to time()
	 *   @option int 'exp'     Expiry date, Unix seconds, defaults to time() + 86400
	 */
	public function __construct($opts = []) {

		if (!empty($opts['sub'])) {
			$this->sub = $opts['sub'];
		} else {
			throw new SisenseGarbageException('user not provided');
		}

		if (!empty($opts['iat'])) {
			$this->iat = $opts['iat'];
		} else {
			$this->iat = time();
		}

		if (!empty($opts['exp'])) {
			$this->exp = $opts['exp'];
		} else {
			$this->exp = time() + (24 * 60 * 60);
		}

		if (!empty($opts['jti'])) {
			$this->jti = $opts['jti'];
		} else {
			$this->randomizeJti();
		}
	}

	public static function getConfig($name = NULL) {
		$app = getApp();
		
		if ($name == NULL) {
			return $app->getConfig()['sisense'];
		} else {
			if (isset($app->getConfig()['sisense'][$name])) {
				return $app->getConfig()['sisense'][$name];
			}
			return NULL;
		}
	}

	public function getTokenData() {
		return array(
			'iat' => $this->iat,
			'sub' => $this->sub,
			'jti' => $this->jti,
			'exp' => $this->exp,
		);
	}

	private function randomizeJti() {
		if (empty($this->jti)) {
			$this->jti = base64_encode(openssl_random_pseudo_bytes(36));
		}
	}

	public function getToken() {
		$config = self::getConfig();

		return JWT::encode(
			$this->getTokenData(),
			$config['secret'],
			['alg' => 'HS256']
		);	
	}

	public function getLocalLoginQuery($return_to = NULL) {
		$config = self::getConfig();
		if (empty($return_to)) {
			$return_to = $config['url'];
		}

		return array(
			'jwt' => $this->getToken(),
			'return_to' => $return_to, // get from Sisense login session
		);
	}

	public function getLocalLoginUrl($return_to = NULL) {
		$config = self::getConfig();
		return $config['url'] . '/jwt?' . http_build_query($this->getLocalLoginQuery($return_to));
	}
}
