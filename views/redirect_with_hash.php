<?php
	render('head.php');
?>
<body class="redirect">
<div class="container">
<p><a id="redirect_link" href="<?= $redirect_url ?>">Click here</a> if your browser does not redirect you <span id="countdown"></span></p>
</div>
<script type="text/javascript">
var n = 2;
var redirect_url = "<?= $redirect_url ?>";
var redirect_append = "";
if (window.location.search) {
	// get the href query with hash
	var hash_query = '?' + window.location.href.split('?').slice(1).join('?');

	// get the part of the href query before return_to=
	var literal_append = hash_query.split("return_to=").slice(0,1).join();
	redirect_append += literal_append;

	// get the part of the href query after return_to=
	var quoted_append = hash_query.split("return_to=").slice(1).join();
	if (quoted_append) {
		redirect_append += "return_to=" + encodeURIComponent(quoted_append);
	}
}
redirect_url += redirect_append;

function initCount(n) {
	updateCountdown(n);
	interval = setInterval( function() { updateCountdown(); }, 1000);
	document.getElementById('redirect_link').href = redirect_url;
}
function updateCountdown() {
	if (n >= 1) {
		document.getElementById('countdown').innerHTML = "in " + n + " seconds";
		n--;
	} else {
		document.getElementById('countdown').innerHTML = '';
		clearInterval(interval);
		window.location = redirect_url;
	}
}

initCount(n);
</script>
</body>
</html>
