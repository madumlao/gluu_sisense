<?php
	header("Location: $redirect_url");
	render('head.php');
?>
<body class="redirect">
<div class="container">
<p><a href="<?= $redirect_url ?>">Click here</a> if your browser does not redirect you <span id="countdown"></span></p>
</div>
<script type="text/javascript">
var n = 5;
var redirect_url = "<?= $redirect_url ?>";

function initCount(n) {
	updateCountdown(n);
	interval = setInterval( function() { updateCountdown(); }, 1000);
}
function updateCountdown() {
	if (n >= 1) {
		document.getElementById('countdown').innerHTML = "in " + n + " seconds";
		n--;
	} else {
		document.getElementById('countdown').innerHTML = '';
		clearInterval(interval);
		window.location = redirect_url;
	}
}

initCount(n);
</script>
</body>
</html>
