<?php
	require_once dirname(__FILE__) . '/lib/common.php';
	$app = getApp();
?>
<?php
	$redirect_url = 'oic/login.php';
	render('redirect_with_hash.php', [
		"redirect_url" => $redirect_url,
	]);
