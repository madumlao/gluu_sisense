<?php
	// called by client to redirect to gluu
	require_once __DIR__ . '/../lib/init.php';

	if (!empty($_REQUEST['return_to'])) {
		// generate a redirect URL based on return to URL

		$_SESSION['sisense']['return_to'] = $_REQUEST['return_to'];
	}

	if (empty($_SESSION['oic_session_id'])) {
		$oic_session = new OicSession;
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	} else {
		try {
			$oic_session = OicSession::find($_SESSION['oic_session_id']);
		} catch (ActiveRecord\RecordNotFound $e) {
			$oic_session = new OicSession;
		}

		if ($oic_session->isComplete() && $oic_session->isExpired()) {
			try {
				$oic_session->refreshAccessToken();
			} catch (OicErrorException $e) {
				$oic_session->delete();
				$oic_session = new OicSession;
			}
		}
		$oic_session->save();
		
		$_SESSION['oic_session_id'] = $oic_session->attributes()['id'];
	}
	
	if ($oic_session->isComplete()) {
		$logged_in = TRUE;
		// user already logged in
		$claims = $oic_session->getClaims();
		$sisense = new Sisense([
			'sub' => $claims['email'],
			'iat' => $claims['iat'],
			'exp' => $claims['exp'],
		]);

		if (!empty($_SESSION['sisense']['return_to'])) {
			$return_to = $_SESSION['sisense']['return_to'];
		} else {
			$return_to = NULL;
		}

		$redirect_url = $sisense->getLocalLoginUrl($return_to);
	} else {
		// user not yet logged in
		$logged_in = NULL;
		$redirect_url = $oic_session->getAuthorizationUrl();
	}

	// perform the redirect
	redirect($redirect_url);
