<?php
	require_once dirname(dirname(__FILE__)) . '/lib/init.php';

	if (!empty($_SESSION['oic_session_id'])) {
		try {
			$oic_session = OicSession::find($_SESSION['oic_session_id']);
			$oic_session->delete();
		} catch (ActiveRecord\RecordNotFound $e) {
			// do nothing
		}
	}

	session_destroy();
	render('loggedout.php');
