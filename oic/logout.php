<?php
	require_once __DIR__ . '/../lib/init.php';
	if (empty($_SESSION['oic_session_id'])) {
		// no openid session, logout the local account
		$redirect_url = appUrl('/oic/local_logout.php');
	}
	
	try {
		$oic_session = OicSession::find($_SESSION['oic_session_id']);
		$redirect_url = $oic_session->getEndSessionUrl();
		$oic_session->delete();
	} catch (ActiveRecord\RecordNotFound $e) {
		$redirect_url = appUrl('/oic/local_logout.php');
	}

	session_destroy();
	redirect($redirect_url);
